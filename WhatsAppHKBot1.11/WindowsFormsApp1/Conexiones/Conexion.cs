﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace WindowsFormsApp1.Conexiones
{
    class Conexion
    {
        private string c_server = "127.0.0.1";
        private string c_base = "base";
        private string c_id = "sa";
        private string c_pas = "123456";
        public SqlConnection cn = new SqlConnection();
        DataTable tablaLista = new DataTable();

        public object CARTAB { get; private set; }

        public void connection()
        {


            
            if (cn.State == ConnectionState.Open)
            {
                cn.Close();
            } else
                try
                {
                    cn.ConnectionString = "data source=" + c_server + "; database=" + c_base + " ; user id=" + c_id + "; password= " + c_pas + " ";
                    cn.Open();
                    //MessageBox.Show("Se abrió la conexión con el servidor SQL Server y se seleccionó la base de datos");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }


        }

        public DataTable CARGAR_TABLA(string CONS)
        {
            try
            {
                SqlDataAdapter CARTAB;
                DataTable tabla = new DataTable();
                connection();
                CARTAB = new SqlDataAdapter(CONS, cn);
                CARTAB.Fill(tabla);
                DESCONECTAR();
                return tabla;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void DESCONECTAR()
        {
            if (cn.State == ConnectionState.Open)
            {
                cn.Close();
            }
        }

        public int EJECUTAR_QUERY(string cadena)
        {
            try
            {
                SqlCommand eje;
                int id;
                connection();
                eje = new SqlCommand(cadena, cn);
                eje.CommandTimeout = 900;
                id = eje.ExecuteNonQuery();
                eje = null;
                DESCONECTAR();
                return 1;
            }
            catch
            {
                return 0;
            }

        }


        public DataTable CARGAR_TABLA_Lista(string CONS)
        {
            try
            {
                SqlDataAdapter CARTAB;

                connection();
                CARTAB = new SqlDataAdapter(CONS, cn);
                CARTAB.Fill(tablaLista);
                DESCONECTAR();
                return tablaLista;
            }
            catch (Exception e)
            {
                return null;
            }
        } 

        public void limpiarTablaLista()
        {
            tablaLista = new DataTable();
        }


        public string DEVUELVE_UN_VALOR(string CONS ) 
        {
            try
            {
                connection();
                SqlCommand newcod;  
                SqlDataReader dato; 
                newcod = new SqlCommand(CONS, cn);
                dato = newcod.ExecuteReader();
                while (dato.Read())
                {    
                    return dato[0].ToString();
                }
                DESCONECTAR();
                return "";
            }
            catch
            {
                return "";
            }
        }


    }
}
