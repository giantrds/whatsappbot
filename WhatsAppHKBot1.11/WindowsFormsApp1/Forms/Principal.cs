﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using WindowsFormsApp1.Conexiones;

namespace WindowsFormsApp1
{


    public partial class Fine : Form
    {


        IWebDriver driver;
        Conexion lsql = new Conexion();
        string nunmer;


        public Fine()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CargarGrupos(TxtGrupos.Text);
        }


        public void Numero(string num)
        {
            nunmer = num;
        }


        private bool CheckLoggedIn()
        {
            try
            {
                return driver.FindElement(By.ClassName("_2Uo0Z")).Displayed;

            }
            catch (Exception e)
            {
                Debug.Print(e.Message);
                return false;
            }



        }

        private void SendMessage(string Destinario, string mensaje, string path)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10); //Wait for maximun of 10 seconds if any element is not found

            //driver.Navigate().GoToUrl("https://api.whatsapp.com/send?phone=" + number + "&text=" + Uri.EscapeDataString(message));
            //driver.FindElement(By.Id("action-button")).Click(); // Click SEND Buton

            try
            {

                driver.FindElement(By.CssSelector("input[class=  'jN-F5 copyable-text selectable-text']")).SendKeys(Destinario);
                System.Threading.Thread.Sleep(250);
                driver.FindElement(By.XPath("//span[@title = '" + Destinario + "']")).Click();
                driver.FindElement(By.CssSelector("div[class=  '_2S1VP copyable-text selectable-text']")).SendKeys(mensaje);

                driver.FindElement(By.CssSelector("button._35EW6")).Click();//Click SEND Arrow Button

                //Enviar imagen o video

                if (dgvRuta.Rows.Count != 0)
                {
                    driver.FindElement(By.CssSelector("div[title= 'Adjuntar']")).Click();
                    for (int i = 0; i < dgvRuta.Rows.Count; i++)
                    {
                        path = dgvRuta.Rows[i].Cells[0].Value.ToString();
                        if (i == 0)
                        {
                            driver.FindElement(By.CssSelector("input[accept = 'image/*,video/mp4,video/3gpp,video/quicktime']")).SendKeys(path);
                        }
                        else
                        {
                            driver.FindElement(By.CssSelector("input[accept = '*']")).SendKeys(path);
                        }
                        System.Threading.Thread.Sleep(1500);
                    }
                    driver.FindElement(By.CssSelector("div[class = '_3hV1n yavlE']")).Click();
                }
                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }


        public void run()
        {

            if (dgvLista.Rows.Count !=0)
            { 
                driver = new ChromeDriver();
                driver.Manage().Window.Maximize();
                driver.Navigate().GoToUrl("https://web.whatsapp.com");
                while (true)
                {
                    //Console.WriteLine("Login to WhatsApp Web and Press Enter");
                    //Console.ReadLine();
                    if (CheckLoggedIn())
                        break;
                }

            
                string Mensaje = txtMensaje.Text;
                string destinatario;
                //string Ruta = txtRuta.Text;
                for (int i = 0; i < dgvLista.Rows.Count; i++)
                {
                    destinatario = dgvLista.Rows[i].Cells[1].Value.ToString();
                    SendMessage(destinatario, Mensaje, "");
                    System.Threading.Thread.Sleep(750);
                }
                MessageBox.Show("Mensajes enviados exitosamente...!");
            }
            else
            {
                MessageBox.Show("Agregue los grupos a la lista antes de enviar los mensajes...!");
            }
            //*** Replace here ***//
        }

        private void BtnNumero_Click(object sender, EventArgs e)
        {
            try
            {
                run();
            }
            catch
            {
                MessageBox.Show("Contacte al administrador...!");
            }

            
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            AdjuntarArchivo();
        }

        private void AdjuntarArchivo()
        {
            //openFileDialog1.Filter = "image(*.*)|video(*.mp4)";
            openFileDialog1.Multiselect = true;
            DialogResult dr = this.openFileDialog1.ShowDialog();
            string[] result = openFileDialog1.FileNames;
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                foreach (String file in result)
                {

                    int rows = dgvRuta.Rows.Count;
                    dgvRuta.Rows.Add();
                    dgvRuta.Rows[rows].Cells[0].Value = file;
                    EliminarDuplicados(dgvRuta);
                }

            }
        }

        private void EliminarDuplicados(DataGridView dgv)
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                for (int y = i + 1; y < dgv.Rows.Count; y++)
                {
                    if (dgv.Rows[i].Cells[0].Value.Equals(dgv.Rows[y].Cells[0].Value))
                    {
                        dgv.Rows.RemoveAt(i);
                    }
                }
            }


        }

        private void CargarGrupos(string filtro)
        {
            dgvGrupos.DataSource = null;
            string numero = nunmer;
            string Query = "Select id id,Name name from Contacs where Name like '" + filtro + "%' and number = '" + numero + "'";
            dgvGrupos.DataSource = lsql.CARGAR_TABLA(Query);
           

        }

        private void AgregarGrupos()
        {

            if (TxtGrupos.Text != "")
            {
                string Query = "insert into Contacs(name,number) Values('" + TxtGrupos.Text + "','"+ nunmer +"')";
                if (lsql.EJECUTAR_QUERY(Query) == 1)
                {
                    TxtGrupos.Text = "";
                    CargarGrupos(TxtGrupos.Text);
                    MessageBox.Show("Se agrego correctamente...!");
                }
            }else
            {
                MessageBox.Show("Complete el nombre del Grupo...!");
                TxtGrupos.Focus();
            }
            
            
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            AgregarGrupos();
        }

        private void BtnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarLista();
        }

        private void LimpiarLista()
        {
            dgvLista.DataSource = null;
            lsql.limpiarTablaLista();
        }

        private void AgregarLista()
        {
            string Query = "Select id id,Name name from Contacs where Name like '%" + txtLista.Text + "%' and number = '"+nunmer +"'";
            dgvLista.DataSource = lsql.CARGAR_TABLA_Lista(Query);
            EliminarDuplicados(dgvLista);
        }

        private void BtnAgregarLista_Click(object sender, EventArgs e)
        {
            AgregarLista();
        }

        private void DgvGrupos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dgvGrupos.Rows.Count != 0)
            {

                if (MessageBox.Show("¿Seguro que desea eliminar el registro?", "Salir", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    string Query = "delete from Contacs where id = '" + dgvGrupos.CurrentRow.Cells[0].Value + "'";
                    if (lsql.EJECUTAR_QUERY(Query) == 1)
                    {
                        TxtGrupos.Text = "";
                        CargarGrupos(TxtGrupos.Text);
                        LimpiarLista();
                        MessageBox.Show("Se elimino Correctamente...!");
                    }
                }
                
            }
        }

        private void DgvRuta_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dgvRuta.Rows.Count != 0)
            {
                dgvRuta.Rows.RemoveAt(dgvRuta.CurrentRow.Index);
            }
            
        }
    }
}

